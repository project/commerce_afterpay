# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Fixed

- Incorrect line item price sent to Afterpay for quantities > 1 [#3395676].

### Changed

- Support Drupal 10 [#3393315].

## [1.0.0-alpha5] - 2022-08-25

### Fixed

- Exclude Afterpay from orders with a different currency [#3254344].

### Changed

- Support PHP 8.x [#3301307].

## [1.0.0-alpha4] - 2021-09-16

### Changed

- Improve exception handling [#3232779].
- Remove `$validate` parameter from `sendRequest`
  (No longer needed - see https://github.com/afterpay/sdk-php/issues/13).

## [1.0.0-alpha3] - 2021-09-16

### Changed

- Add afterpay-php-sdk versions 1.3.0 - 1.3.6 to
  the `composer.json` `conflict` section. Removed unsetting of
  `merchantPortalOrderUrl` now that issue
  with [missing property in Payment model](https://github.com/afterpay/sdk-php/issues/42)
  has been resolved [#3232783].

## [1.0.0-alpha2] - 2021-09-08

### Fixed

- Fix ApiIntegrationTest now
  that [shipping auto-recalculates on checkout][#2849756].
- Fix error on deferred auth with afterpay/sdk-php 1.3.0+ [#3231706].

### Changed

- Refund requests are now idempotent [#3208435]

## [1.0.0-alpha1] - 2021-04-21

### Added

- Initial release supporting deferred payments.

#####

[#3232779]: https://www.drupal.org/project/commerce_afterpay/issues/3232779

[#3232783]: https://www.drupal.org/project/commerce_afterpay/issues/3232783

[#2849756]: https://www.drupal.org/project/commerce_shipping/issues/2849756

[#3208435]: https://www.drupal.org/project/commerce_afterpay/issues/3208435

[#3231706]: https://www.drupal.org/project/commerce_afterpay/issues/3231706

[#3254344]: https://www.drupal.org/project/commerce_afterpay/issues/3254344

[#3301307]: https://www.drupal.org/project/commerce_afterpay/issues/3301307

[#3393315]: https://www.drupal.org/project/commerce_afterpay/issues/3393315

[#3395676]: https://www.drupal.org/project/commerce_afterpay/issues/3395676

######

[Unreleased]: https://git.drupalcode.org/project/commerce_afterpay/-/compare/1.0.0-alpha5...1.x

[1.0.0-alpha5]: https://git.drupalcode.org/project/commerce_afterpay/-/compare/1.0.0-alpha4...1.0.0-alpha5

[1.0.0-alpha4]: https://git.drupalcode.org/project/commerce_afterpay/-/compare/1.0.0-alpha3...1.0.0-alpha4

[1.0.0-alpha3]: https://git.drupalcode.org/project/commerce_afterpay/-/compare/1.0.0-alpha2...1.0.0-alpha3

[1.0.0-alpha2]: https://git.drupalcode.org/project/commerce_afterpay/-/compare/1.0.0-alpha1...1.0.0-alpha2

[1.0.0-alpha1]: https://git.drupalcode.org/project/commerce_afterpay/-/tree/1.0.0-alpha1
