<?php declare(strict_types=1);

namespace Drupal\Tests\commerce_afterpay\FunctionalJavascript;

use Drupal\commerce_order\Entity\OrderType;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\Tests\commerce\FunctionalJavascript\CommerceWebDriverTestBase;

/**
 * Tests the checkout process using Afterpay sandbox details.
 *
 * @group commerce_afterpay
 *
 * @todo Add promotion.
 */
class ApiIntegrationTest extends CommerceWebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_afterpay',
    'commerce_cart',
    'commerce_checkout',
    'commerce_order',
    'commerce_product',
    'commerce_shipping',
  ];

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The email address for the Afterpay customer account.
   *
   * @var string
   */
  protected $customerEmail;

  /**
   * The password for the Afterpay customer account.
   *
   * @var string
   */
  protected $customerPassword;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The product being purchased.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $product;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    // Check for required credentials immediately to skip early if we should.
    $gateway_config = [
      'merchant_id' => getenv('COMMERCE_AFTERPAY_TEST_MERCHANT_ID'),
      'secret_key' => getenv('COMMERCE_AFTERPAY_TEST_SECRET_KEY'),
      'country_code' => getenv('COMMERCE_AFTERPAY_TEST_COUNTRY_CODE'),
    ];
    $this->customerEmail = getenv('COMMERCE_AFTERPAY_TEST_CUSTOMER_EMAIL');
    $this->customerPassword = getenv('COMMERCE_AFTERPAY_TEST_CUSTOMER_PASSWORD');

    if (count(array_filter($gateway_config)) !== 3 || !$this->customerEmail || !$this->customerPassword) {
      $this->markTestSkipped("This test requires Afterpay sandbox credentials in environment variables: see README.md for more details.");
    }

    parent::setUp();

    $this->placeBlock('commerce_cart');
    $this->placeBlock('commerce_checkout_progress');

    $this->store->shipping_countries = ['US'];
    $this->store->save();

    $gateway = PaymentGateway::create([
      'id' => 'afterpay_gateway',
      'label' => 'Afterpay',
      'plugin' => 'afterpay',
    ]);
    assert($gateway instanceof PaymentGateway);

    $gateway->getPlugin()
      ->setConfiguration([
        'mode' => 'test',
        'payment_method_types' => ['credit_card'],
      ] + $gateway_config);
    $gateway->save();

    $product_variation_type = ProductVariationType::load('default');
    $product_variation_type->setTraits(['purchasable_entity_shippable']);
    $product_variation_type->save();

    $order_type = OrderType::load('default');
    $order_type->setThirdPartySetting('commerce_checkout', 'checkout_flow', 'shipping');
    $order_type->setThirdPartySetting('commerce_shipping', 'shipment_type', 'default');
    $order_type->save();

    // Create the order field.
    $field_definition = commerce_shipping_build_shipment_field_definition($order_type->id());
    $this->container->get('commerce.configurable_field_manager')->createField($field_definition);

    // Install the variation trait.
    $trait_manager = $this->container->get('plugin.manager.commerce_entity_trait');
    $trait = $trait_manager->createInstance('purchasable_entity_shippable');
    $trait_manager->installTrait($trait, 'commerce_product_variation', 'default');

    $variation = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => 22.50,
        'currency_code' => 'USD',
      ],
    ]);

    $this->product = $this->createEntity('commerce_product', [
      'type' => 'default',
      'title' => 'My product',
      'variations' => [$variation],
      'stores' => [$this->store],
    ]);

    $this->createEntity('commerce_shipping_method', [
      'name' => 'Shipping',
      'stores' => [$this->store->id()],
      'plugin' => [
        'target_plugin_id' => 'flat_rate',
        'target_plugin_configuration' => [
          'rate_label' => 'Shipping',
          'rate_amount' => [
            'number' => '2.25',
            'currency_code' => 'USD',
          ],
          'default_package_type' => 'custom_box',
        ],
      ],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'administer commerce_payment',
      'administer commerce_order',
    ], parent::getAdministratorPermissions());
  }

  /**
   * Tests than an order can go through checkout steps.
   */
  public function testCheckout() {
    // Test an anonymous user making a checkout.
    $this->drupalLogout();

    $this->drupalGet($this->product->toUrl()->toString());
    $this->submitForm([], 'Add to cart');
    $this->submitForm([], 'Add to cart');

    $this->drupalGet('checkout/1');
    $this->submitForm([], 'Continue as Guest');

    $page = $this->getSession()->getPage();
    $page->fillField('Email', $this->customerEmail);
    $page->fillField('First name', 'Johnny');
    $page->fillField('Last name', 'Appleseed');
    $page->fillField('Street address', '123 New York Drive');
    $page->fillField('City', 'New York City');
    $page->selectFieldOption('State', 'New York');
    $page->fillField('Zip code', '10001');
    $this->assertSession()->waitForField('Shipping: $2.25');
    $this->submitForm([], 'Continue to review');

    usleep(200000);
    $this->submitForm([], 'Pay and complete purchase');

    $password = $this->assertSession()->waitForElementVisible('named', ['field', 'password']);
    $page = $this->getSession()->getPage();
    $this->assertNotNull($password);
    $password->setValue($this->customerPassword);
    $page->pressButton('Log In');
    $ok = $this->assertSession()->waitForButton('Okay, got it!');
    $this->assertNotNull($ok);
    $ok->click();
    // The actual 'terms agreed' checkbox has an element above it, so trying to
    // click it directly doesn't work - instead target the element above it
    // directly.
    $terms_agreed = $this->assertSession()->waitForElement('css', 'label[for=termsAgreed] .checkbox__tick');
    $this->assertNotNull($terms_agreed);
    $terms_agreed->click();
    $confirm = $this->assertSession()->waitForButton('Confirm');
    $this->assertNotNull($confirm);
    $confirm->click();
    $this->assertSession()->waitForText('Your order number is 1', 20000);

    // Test an administrator capturing and voiding payments.
    $this->drupalLogin($this->adminUser);

    // @todo Improve the assertions below - we should be able to assert contents
    //   of a table cell based on heading and row number.
    $this->drupalGet('admin/commerce/orders/1/payments');
    $this->assertSession()->pageTextContains('Authorization');
    $this->assertSession()->pageTextContains('$47.25');

    $this->drupalGet('admin/commerce/orders/1/payments/1/operation/capture');
    $this->submitForm(['Amount' => '10.00'], 'Capture');

    $this->assertSession()->pageTextContains('$37.25');
    $this->assertSession()->pageTextContains('$10.00');
    $this->assertSession()->linkExists('Delete');

    $this->drupalGet('admin/commerce/orders/1/payments/1/operation/capture');
    $this->submitForm(['Amount' => '5.00'], 'Capture');

    $this->assertSession()->pageTextContains('Payment captured');
    $this->assertSession()->pageTextContains('$32.25');
    $this->assertSession()->pageTextContains('$10.00');
    $this->assertSession()->pageTextContains('$5.00');

    $this->drupalGet('admin/commerce/orders/1/payments/1/operation/void');
    $this->assertSession()->pageTextContains('This action cannot be undone.');
    $this->submitForm([], 'Void');

    $this->assertSession()->pageTextContains('Authorization (Voided)');
    $this->assertSession()->pageTextContains('Completed');

    // Partially refund the second payment ($10).
    $this->drupalGet('admin/commerce/orders/1/payments/2/operation/refund');
    $this->submitForm(['Amount' => '7.50'], 'Refund');

    $this->assertSession()->pageTextContains('Payment refunded');
    $this->assertSession()->pageTextContains('Partially refunded');
    $this->assertSession()->pageTextContains('$10.00 Refunded: $7.50');

    // Fully refund the third payment ($5).
    $this->drupalGet('admin/commerce/orders/1/payments/3/operation/refund');
    $this->submitForm(['Amount' => '5.00'], 'Refund');

    $this->assertSession()->pageTextContains('Payment refunded');
    $this->assertSession()->pageTextContains('Refunded');
    $this->assertSession()->pageTextContains('$5.00 Refunded: $5.00');
  }

}
