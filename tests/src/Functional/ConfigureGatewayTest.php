<?php declare(strict_types=1);

namespace Drupal\Tests\commerce_afterpay\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Provides functional tests for configuring the afterpay payment gateway.
 *
 * @group commerce_afterpay
 */
class ConfigureGatewayTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['commerce_afterpay'];

  /**
   * Tests that the configuration page is reachable.
   *
   * @todo Test being passed to client.
   */
  public function testConfigureGateway() {
    $user = $this->createUser(['administer commerce_payment_gateway']);
    $this->drupalLogin($user);

    // Add the gateway.
    $this->drupalGet('admin/commerce/config/payment-gateways/add');
    $this->assertSession()->fieldValueEquals('Merchant ID', '');
    $this->assertSession()->fieldValueEquals('Secret key', '');
    $this->assertSession()->fieldValueEquals('Country code', 'US');
    $this->submitForm([
      'Name' => 'Afterpay',
      'Machine-readable name' => 'afterpay',
      'Merchant ID' => 'MERCHANT ID',
      'Secret key' => 'SECRET KEY',
      'Country code' => 'FR',
    ], 'Save');
    $this->assertSession()->pageTextContains('Saved the Afterpay payment gateway');

    // Check the details were saved and show up when editing the gateway.
    $this->drupalGet('admin/commerce/config/payment-gateways/manage/afterpay');
    $this->assertSession()->fieldValueEquals('Merchant ID', 'MERCHANT ID');
    $this->assertSession()->fieldValueEquals('Secret key', 'SECRET KEY');
    $this->assertSession()->fieldValueEquals('Country code', 'FR');
  }

}
