<?php declare(strict_types=1);

namespace Drupal\Tests\commerce_afterpay\Unit;

use Drupal\commerce_afterpay\Client\Client;
use Drupal\commerce_afterpay\Client\Configuration;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;

/**
 * Tests the configuration object.
 *
 * @coversDefaultClass \Drupal\commerce_afterpay\Client\Configuration
 *
 * @group commerce_afterpay
 */
class ConfigurationTest extends UnitTestCase {

  /**
   * The cache prophecy.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $cache;

  /**
   * The Afterpay client prophecy.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $client;

  /**
   * An object holding the remote configuration in the remote schema.
   *
   * @var object
   */
  protected $config;

  /**
   * The time service prophecy.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->cache = $this->prophesize(CacheBackendInterface::class);
    $this->config = (object) [
      'minimumAmount' => (object) [
        'amount' => '35.00',
        'currency' => 'USD',
      ],
      'maximumAmount' => (object) [
        'amount' => '1000.00',
        'currency' => 'USD',
      ]
    ];
    $this->client = $this->prophesize(Client::class);
    $this->time = $this->prophesize(TimeInterface::class);
    $this->time->getRequestTime()->willReturn(0);
  }

  /**
   * Tests getting the configuration with nothing in the persistent cache.
   *
   * @covers ::getConfiguration
   */
  public function testWithUnprimedCached(): void {
    // @todo Test the CID is the same in both cases.
    // Due to the static cache, the persistent cache should only be queried
    // once.
    $this->cache->get(Argument::cetera())
      ->shouldBeCalledTimes(1)
      ->willReturn(FALSE);
    // The persistent cache should only be set once.
    $this->cache->set(Argument::any(), $this->config, Configuration::CACHE_LIFETIME)
      ->shouldBeCalledTimes(1);
    // Due to caching there should only be one call via the client.
    $this->client->getConfiguration()
      ->shouldBeCalledTimes(1)
      ->willReturn($this->config);

    $configuration = new Configuration('test', $this->client->reveal(), $this->cache->reveal(), $this->time->reveal());
    // The initial call will be satisfied by the client.
    $configuration->getMinimumAmount();
    // The second call will be satisfied by the static cache: there should be no
    // call to the persistent cache or the client.
    $minimum = $configuration->getMinimumAmount();
    $maximum = $configuration->getMaximumAmount();

    $this->assertEquals(new Price('35.00', 'USD'), $minimum);
    $this->assertEquals(new Price('1000.00', 'USD'), $maximum);
  }

  /**
   * Tests getting the configuration with data in the persistent cache.
   *
   * @covers ::getConfiguration
   */
  public function testWithPrimedCache(): void {
    // Use a different result from the client to verify we're getting cached
    // data.
    $cached_result = (object) [
      'data' => (object) [
        'minimumAmount' => (object) [
          'amount' => '45.00',
          'currency' => 'USD',
        ],
        'maximumAmount' => (object) [
          'amount' => '1100.00',
          'currency' => 'USD',
        ]
      ],
    ];
    // Due to the static cache, there should only be one call to set the cache
    $this->cache->get(Argument::any())
      ->shouldBeCalledTimes(1)
      ->willReturn($cached_result);
    // As it's already cached, there shouldn't be any call to set the cache.
    $this->cache->set(Argument::cetera())->shouldNotBeCalled();
    // Due to the persistent cache, there shouldn't be any call via the client.
    $this->client->getConfiguration()->shouldNotBeCalled();

    $configuration = new Configuration('test', $this->client->reveal(), $this->cache->reveal(), $this->time->reveal());
    // The initial call will be satisfied by the persistent cache.
    $configuration->getMinimumAmount();
    // The second call will be satisfied by the static cache.
    $minimum = $configuration->getMinimumAmount();
    $maximum = $configuration->getMaximumAmount();

    $this->assertEquals(new Price('45.00', 'USD'), $minimum);
    $this->assertEquals(new Price('1100.00', 'USD'), $maximum);
  }

  /**
   * Tests getting the configuration when there's no minimum amount.
   */
  public function testWithNoMinimum(): void {
    $config = clone $this->config;
    unset($config->minimumAmount);
    $this->client->getConfiguration()
      ->willReturn($config);

    $configuration = new Configuration('test', $this->client->reveal(), $this->cache->reveal(), $this->time->reveal());
    $minimum = $configuration->getMinimumAmount();
    $maximum = $configuration->getMaximumAmount();

    $this->assertEquals(NULL, $minimum);
    $this->assertEquals(new Price('1000.00', 'USD'), $maximum);
  }

}
