<?php declare(strict_types=1);

namespace Drupal\commerce_afterpay\Client;

use Drupal\Component\Uuid\UuidInterface;

/**
 * Creates the Afterpay client.
 */
class ClientFactory {

  /**
   * The remote model service.
   *
   * @var \Drupal\commerce_afterpay\Client\RemoteModel
   */
  protected $remoteModel;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * Creates the factory.
   *
   * @param \Drupal\commerce_afterpay\Client\RemoteModel $remote_model
   *   The remote model service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID service.
   */
  public function __construct(RemoteModel $remote_model, UuidInterface $uuid) {
    $this->remoteModel = $remote_model;
    $this->uuid = $uuid;
  }

  /**
   * Creates the Afterpay client given the configuration.
   *
   * @param \Drupal\commerce_afterpay\Client\ClientConfig $config
   *   The configuration for the client.
   *
   * @return \Drupal\commerce_afterpay\Client\ClientInterface
   *   The client.
   */
  public function createClient(ClientConfig $config): ClientInterface {
    return new Client($config, $this->remoteModel, $this->uuid);
  }

}
