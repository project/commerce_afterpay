<?php declare(strict_types=1);

namespace Drupal\commerce_afterpay\Client;

use Afterpay\SDK\Exception\InvalidArgumentException;
use Afterpay\SDK\Exception\NetworkException;
use Afterpay\SDK\Exception\ParsingException;
use Afterpay\SDK\Exception\PrerequisiteNotMetException;
use Afterpay\SDK\HTTP\Request;
use Afterpay\SDK\HTTP\Request\CreateCheckout;
use Afterpay\SDK\HTTP\Request\CreateRefund;
use Afterpay\SDK\HTTP\Request\DeferredPaymentAuth;
use Afterpay\SDK\HTTP\Request\DeferredPaymentCapture;
use Afterpay\SDK\HTTP\Request\DeferredPaymentVoid;
use Afterpay\SDK\HTTP\Request\GetConfiguration;
use Afterpay\SDK\HTTP\Response;
use Afterpay\SDK\MerchantAccount;
use Afterpay\SDK\Model\Payment as AfterpayPayment;
use Afterpay\SDK\Model\Refund;
use CommerceGuys\Addressing\AddressInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_payment\Entity\PaymentInterface as CommercePayment;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\InvalidResponseException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_price\Price;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Provides a Drupal client for the Afterpay API.
 */
class Client implements ClientInterface {

  /**
   * The merchant account authorization details.
   *
   * @var \Afterpay\SDK\MerchantAccount
   */
  protected $merchant;

  /**
   * The remote model.
   *
   * @var \Drupal\commerce_afterpay\Client\RemoteModel
   */
  protected $remoteModel;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidGenerator;

  /**
   * Creates the client.
   *
   * @param \Drupal\commerce_afterpay\Client\ClientConfig $config
   *   The client configuration.
   * @param \Drupal\commerce_afterpay\Client\RemoteModel $remote_model
   *   The remote model.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID service.
   */
  public function __construct(ClientConfig $config, RemoteModel $remote_model, UuidInterface $uuid) {
    $this->merchant = new MerchantAccount();
    try {
      $this->merchant->setMerchantId($config->getMerchantId())
        ->setSecretKey($config->getSecretKey())
        ->setCountryCode($config->getCountryCode())
        ->setApiEnvironment($config->getMode() === 'live' ? 'production' : 'sandbox');
    }
    catch (InvalidArgumentException $e) {
      throw new PaymentGatewayException($e->getMessage(), $e->getCode(), $e);
    }

    $this->uuidGenerator = $uuid;
    $this->remoteModel = $remote_model;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): object {
    $request = new GetConfiguration([]);
    return $this->sendRequest($request)->getParsedBody();
  }

  /**
   * {@inheritdoc}
   */
  public function createCheckout(OrderInterface $order, string $confirm_url, string $cancel_url): object {
    $profiles = $order->collectProfiles();
    // @todo Confirm Afterpay doesn't support non-physical purchases.
    if (empty($profiles['shipping'])) {
      throw new PaymentGatewayException("Afterpay requires a shipping address.");
    }

    ['billing' => $billing, 'shipping' => $shipping] = $profiles;
    assert($billing instanceof ProfileInterface);
    assert($shipping instanceof ProfileInterface);
    $billing_address = $billing->address->first();
    $shipping_address = $shipping->address->first();
    assert($billing_address instanceof AddressInterface);
    assert($shipping_address instanceof AddressInterface);

    $product_items = array_filter($order->getItems(), function (OrderItemInterface $item): bool {
      return $item->hasPurchasedEntity();
    });

    $afterpay_items = array_map([
      $this->remoteModel,
      'createItem',
    ], $product_items);

    $request = new CreateCheckout();
    $request
      ->setMerchantReference($order->id())
      ->setAmount($this->remoteModel->createMoney($order->getTotalPrice()))
      ->setConsumer($this->remoteModel->createConsumer($order, $billing))
      ->setBilling($this->remoteModel->createContact($billing))
      ->setShipping($this->remoteModel->createContact($shipping))
      ->setItems($afterpay_items)
      ->setMerchant([
        'redirectConfirmUrl' => $confirm_url,
        'redirectCancelUrl' => $cancel_url,
      ]);

    // Add discounts.
    $promotions = $order->collectAdjustments(['promotion']);
    if ($promotions) {
      $request->setDiscounts(array_map([
        $this->remoteModel,
        'createDiscount',
      ], $promotions));
    }

    // Add tax and shipping.
    $adjustments = [];
    // @todo This assumes only one shipping and tax adjustment: is that safe in
    //   the general case? Should we reduce them down to one?
    foreach ($order->collectAdjustments(['shipping', 'tax']) as $adjustment) {
      $adjustments[$adjustment->getType()] = $adjustment;
    }
    if (!empty($adjustments['tax'])) {
      $tax_money = $this->remoteModel->createMoney($adjustments['tax']->getAmount());
      $request->setTaxAmount($tax_money);
    }
    if (!empty($adjustments['shipping'])) {
      $shipping_money = $this->remoteModel->createMoney($adjustments['shipping']->getAmount());
      $request->setShippingAmount($shipping_money);
    }

    return $this->sendRequest($request)->getParsedBody();
  }

  /**
   * {@inheritdoc}
   */
  public function auth(string $token): AfterpayPayment {
    $uuid = $this->uuidGenerator->generate();
    $request = new DeferredPaymentAuth([
      'token' => $token,
      'requestId' => $uuid,
    ]);

    try {
      $body = $this->sendRequest($request)->getParsedBody();
      $payment = new AfterpayPayment($body);
    }
    catch (PaymentGatewayException $e) {
      throw new DeclineException($e->getMessage(), $e->getCode(), $e);
    }
    return $payment;
  }

  /**
   * {@inheritdoc}
   */
  public function capture(CommercePayment $payment, Price $amount): AfterpayPayment {
    $request = new DeferredPaymentCapture([
      'requestId' => $this->uuidGenerator->generate(),
      'merchantReference' => $payment->getOrderId(),
      'amount' => $this->remoteModel->createMoney($amount),
    ]);
    $this->setOrderIdOnRequest($request, $payment);
    $response = $this->sendRequest($request);
    return new AfterpayPayment($response->getParsedBody());
  }

  /**
   * {@inheritdoc}
   */
  public function void(CommercePayment $payment): AfterpayPayment {
    $request = new DeferredPaymentVoid([
      'amount' => $this->remoteModel->createMoney($payment->getAmount()),
      'requestId' => $this->uuidGenerator->generate(),
    ]);
    $this->setOrderIdOnRequest($request, $payment);
    $response = $this->sendRequest($request);
    return new AfterpayPayment($response->getParsedBody());
  }

  /**
   * {@inheritdoc}
   */
  public function refund(CommercePayment $payment, Price $amount = NULL): Refund {
    // While Afterpay accepts a refund against the order as a whole, Drupal
    // Commerce ties it to a particular payment.
    $amount = $amount ?: $payment->getAmount();
    $request = new CreateRefund([
      'amount' => $this->remoteModel->createMoney($amount),
      'requestId' => $this->uuidGenerator->generate(),
      'merchantReference' => $payment->getOrderId(),
      // @todo Implement refundMerchantReference.
    ]);
    $this->setOrderIdOnRequest($request, $payment);
    $response = $this->sendRequest($request);
    return new Refund($response->getParsedBody());
  }

  /**
   * Performs a request, injecting credentials and handling some errors.
   *
   * @param \Afterpay\SDK\HTTP\Request $request
   *   The request to be made.
   *
   * @return \Afterpay\SDK\HTTP\Response
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   * @throws \Drupal\commerce_payment\Exception\InvalidResponseException
   *
   * @todo Add platform details.
   */
  protected function sendRequest(Request $request): Response {
    try {
      $request->setMerchantAccount($this->merchant);
    }
    catch (InvalidArgumentException $e) {
      throw new PaymentGatewayException($e->getMessage(), $e->getCode(), $e);
    }

    if (!$request->isValid()) {
      $context = [
        '@uri' => $request->getUri(),
        '@messages' => $request->getValidationErrors(),
      ];
      $message = t("Invalid data for request to @uri: @messages.", $context);
      throw new PaymentGatewayException($message);
    }

    try {
      $request->send();
    }
    catch (InvalidArgumentException | ParsingException | PrerequisiteNotMetException $e) {
      throw new PaymentGatewayException($e->getMessage(), $e->getCode(), $e);
    }
    catch (NetworkException $e) {
      throw new InvalidResponseException($e->getMessage(), $e->getCode(), $e);
    }

    $response = $request->getResponse();

    if (!$response->isSuccessful()) {
      $body = $response->getParsedBody();
      throw new PaymentGatewayException($body->message, $body->errorCode);
    }

    return $response;
  }

  /**
   * Sets order ID on request.
   *
   * Must be done separately. Order ID is a path parameter in the Request, and
   * thus is expected to be set using the setOrderId method, whereas Request
   * body parameters can be passed via the constructor.
   *
   * @see https://github.com/afterpay/sdk-php/issues/11
   *
   * @param \Afterpay\SDK\HTTP\Request\DeferredPaymentCapture|\Afterpay\SDK\HTTP\Request\DeferredPaymentVoid|\Afterpay\SDK\HTTP\Request\CreateRefund $request
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  protected function setOrderIdOnRequest(Request $request, CommercePayment $payment): void {
    $order_id = $payment->getOrder()->getData('commerce_afterpay_order_id');
    try {
      $request->setOrderId($order_id);
    }
    catch (InvalidArgumentException $e) {
      throw new PaymentGatewayException($e->getMessage(), $e->getCode(), $e);
    }
  }

}
