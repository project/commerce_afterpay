<?php declare(strict_types=1);

namespace Drupal\commerce_afterpay\Client;

use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Provides access to the API's remote configuration, caching the results.
 */
class Configuration {

  /**
   * The amount of time in seconds the configuration is cached for.
   */
  const CACHE_LIFETIME = 60 * 60 * 24;

  /**
   * The persistent cache for the Afterpay config.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The Afterpay client.
   *
   * @var \Drupal\commerce_afterpay\Client\ClientInterface
   */
  protected $client;

  /**
   * The statically cached Afterpay config.
   *
   * @var object
   */
  protected $config;

  /**
   * A unique ID for the connection details.
   *
   * @var string
   */
  protected $id;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Creates the configuration object.
   *
   * @param string $id
   *   A unique ID for caching the connection details.
   * @param \Drupal\commerce_afterpay\Client\ClientInterface $client
   *   The Afterpay client.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache for the Afterpay config.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(string $id, ClientInterface $client, CacheBackendInterface $cache, TimeInterface $time) {
    $this->id = $id;
    $this->client = $client;
    $this->cache = $cache;
    $this->time = $time;
  }

  /**
   * Returns the minimum order total required to use Afterpay.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The minimum price if there is one; NULL otherwise.
   */
  public function getMinimumAmount(): ?Price {
    $minimum = $this->getConfiguration()->minimumAmount ?? NULL;
    return $minimum ? new Price($minimum->amount, $minimum->currency) : NULL;
  }

  /**
   * Returns the maximum order total supported by Afterpay.
   *
   * @return \Drupal\commerce_price\Price
   *   The maximum price.
   */
  public function getMaximumAmount(): Price {
    $maximum = $this->getConfiguration()->maximumAmount;
    return new Price($maximum->amount, $maximum->currency);
  }

  /**
   * Get the configuration, caching it both statically and persistently.
   *
   * @return object
   */
  protected function getConfiguration(): object {
    // I suspect the SDK might get updated to have an explicit response, but for
    // now it doesn't appear so.
    if (!$this->config) {
      $cid = 'commerce_afterpay.config:' . $this->id;
      $cache_item = $this->cache->get($cid);
      if ($cache_item) {
        $this->config = $cache_item->data;
      }
      if (!$this->config) {
        $this->config = $this->client->getConfiguration();
        $now = $this->time->getRequestTime();
        // QUESTION: Can we use the merchant ID as a key? Presumably not if one
        //  ID can be shared across different envs (live/dev) or regions.
        // @todo Make the cache lifetime configurable.
        $this->cache->set($cid, $this->config, $now + static::CACHE_LIFETIME);
      }
    }
    return $this->config;
  }

}
