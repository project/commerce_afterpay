<?php declare(strict_types=1);

namespace Drupal\commerce_afterpay\Client;

/**
 * Provides required configuration for the Afterpay client.
 */
final class ClientConfig {

  /**
   * @var string
   */
  protected $countryCode;

  /**
   * @var string
   */
  protected $merchantId;

  /**
   * The environment: live or test.
   *
   * @var string
   */
  protected $mode;

  /**
   * @var string
   */
  protected $secretKey;

  public function __construct(string $merchant_id, string $secret_key, string $country_code, string $mode) {
    $this->countryCode = $country_code;
    $this->merchantId = $merchant_id;
    $this->mode = $mode;
    $this->secretKey = $secret_key;
  }

  public function getCountryCode(): string {
    return $this->countryCode;
  }

  public function getMerchantId(): string {
    return $this->merchantId;
  }

  public function getMode(): string {
    return $this->mode;
  }

  public function getSecretKey(): string {
    return $this->secretKey;
  }

}
