<?php declare(strict_types=1);

namespace Drupal\commerce_afterpay\Client;

use Afterpay\SDK\Model\Payment as AfterpayPayment;
use Afterpay\SDK\Model\Refund;
use Drupal\commerce_order\Entity\OrderInterface;
// Use an alias to help differentiate Afterpay from Commerce payments.
use Drupal\commerce_payment\Entity\PaymentInterface as CommercePayment;
use Drupal\commerce_price\Price;

/**
 * Interface ClientInterface
 *
 * Provides a Drupal client for the Afterpay API.
 *
 * Primarily a wrapper for the Afterpay SDK API that translates between the
 * Drupal and Afterpay model.
 */
interface ClientInterface {

  /**
   * Gets the (remote) configuration.
   *
   * @return object
   *
   * @see https://developers.afterpay.com/afterpay-online/reference#get-configuration
   */
  public function getConfiguration(): object;

  /**
   * Creates a checkout.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The Commerce order.
   * @param string $confirm_url
   *   The confirm URL.
   * @param string $cancel_url
   *   The cancel URL.
   *
   * @return object
   *   The response
   *
   * @see https://developers.afterpay.com/afterpay-online/reference#create-checkout
   */
  public function createCheckout(OrderInterface $order, string $confirm_url, string $cancel_url): object;

  /**
   * Performs an auth request.
   *
   * @param string $token
   *
   * @return \Afterpay\SDK\Model\Payment
   *
   * @throws \Drupal\commerce_payment\Exception\InvalidResponseException
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *
   * @see https://developers.afterpay.com/afterpay-online/reference#auth
   */
  public function auth(string $token): AfterpayPayment;

  /**
   * Performs a capture request.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   * @param \Drupal\commerce_price\Price $amount
   *
   * @return \Afterpay\SDK\Model\Payment
   *
   * @throws \Drupal\commerce_payment\Exception\InvalidResponseException
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *
   * @see https://developers.afterpay.com/afterpay-online/reference#capture-payment
   */
  public function capture(CommercePayment $payment, Price $amount): AfterpayPayment;

  /**
   * Perform a void request.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The Commerce payment to void.
   *
   * @return \Afterpay\SDK\Model\Payment
   *
   * @throws \Drupal\commerce_payment\Exception\InvalidResponseException
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public function void(CommercePayment $payment): AfterpayPayment;

  /**
   * Performs a create refund request.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The Drupal payment to be refunded.
   * @param \Drupal\commerce_price\Price|null $amount
   *   (optional) The amount to refund; defaults to the full payment amount.
   *
   * @return \Afterpay\SDK\Model\Refund
   *   The Afterpay refund object.
   *
   * @throws \Drupal\commerce_payment\Exception\InvalidResponseException
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *
   * @see https://developers.afterpay.com/afterpay-online/reference#create-refund
   */
  public function refund(CommercePayment $payment, Price $amount = NULL): Refund;

}

