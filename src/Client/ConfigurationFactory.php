<?php declare(strict_types=1);

namespace Drupal\commerce_afterpay\Client;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Creates the remote configuration object.
 */
class ConfigurationFactory {

  /**
   * The cache for Afterpay config.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The Afterpay client factory.
   *
   * @var \Drupal\commerce_afterpay\Client\ClientFactory
   */
  protected $clientFactory;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Creates the factory.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache for Afterpay config.
   * @param \Drupal\commerce_afterpay\Client\ClientFactory $client_factory
   *   The Afterpay client factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(CacheBackendInterface $cache, ClientFactory $client_factory, TimeInterface $time) {
    $this->cache = $cache;
    $this->clientFactory = $client_factory;
    $this->time = $time;
  }

  /**
   * Gets the configuration object.
   *
   * @param string $id
   *   A unique identifier used for caching configuration.
   * @param \Drupal\commerce_afterpay\Client\ClientConfig $client_config
   *   The Afterpay client config, stored with the gateway.
   *
   * @return \Drupal\commerce_afterpay\Client\Configuration
   *   The configuration service.
   */
  public function getConfiguration(string $id, ClientConfig $client_config): Configuration {
    $client = $this->clientFactory->createClient($client_config);
    return new Configuration($id, $client, $this->cache, $this->time);
  }

}
