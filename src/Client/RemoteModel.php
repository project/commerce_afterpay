<?php declare(strict_types=1);

namespace Drupal\commerce_afterpay\Client;

use Afterpay\SDK\Model\Consumer;
use Afterpay\SDK\Model\Contact;
use Afterpay\SDK\Model\Discount;
use Afterpay\SDK\Model\Item;
use Afterpay\SDK\Model\Money;
use CommerceGuys\Addressing\AddressInterface;
use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Creates Afterpay entities from Drupal ones.
 */
class RemoteModel {

  public function createContact(ProfileInterface $profile): Contact {
    $address = $profile->address->first();
    assert($address instanceof AddressInterface);
    return new Contact([
      'name' => $address->getGivenName() . ' ' . $address->getFamilyName(),
      'line1' => $address->getAddressLine1(),
      'line2' => $address->getAddressLine2(),
      'area1' => $address->getLocality(),
      'region' => $address->getAdministrativeArea(), // State for US
      'postcode' => $address->getPostalCode(),
      'countryCode' => $address->getCountryCode(),
      // @todo Add event to populate 'phoneNumber'.
    ]);
  }

  public function createConsumer(OrderInterface $order, ProfileInterface $profile): Consumer {
    $address = $profile->address->first();
    assert($address instanceof AddressInterface);
    $given_names = array_filter([$address->getGivenName(), $address->getAdditionalName()]);
    return new Consumer([
      'givenNames' => join(' ', $given_names),
      'surname' => $address->getFamilyName(),
      'email' => $order->getEmail(),
      // @todo Add event to populate 'phoneNumber'.
    ]);
  }

  public function createDiscount(Adjustment $discount): Discount {
    return new Discount([
      'displayName' => $discount->getLabel(),
      'amount' => $this->createMoney($discount->getAmount()),
    ]);
  }

  /**
   * It can only handle order items linked to product variations.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $item
   *
   * @return \Afterpay\SDK\Model\Item
   */
  public function createItem(OrderItemInterface $item): Item {
    $variation = $item->getPurchasedEntity();
    if (!$variation instanceof ProductVariationInterface) {
      throw new \RuntimeException(t("Can't create Afterpay item from non-product order item."));
    }
    return new Item([
      'name' => $item->label(),
      'sku' => $variation->getSku(),
      'quantity' => intval($item->getQuantity()),
      'pageUrl' => $variation->toUrl()->toString(),
      'price' => $this->createMoney($item->getUnitPrice()),
      // @todo Add event to populate 'imageUrl' and 'categories'.
    ]);
  }

  public function createMoney(Price $price): Money {
    return new Money([
      'amount' => $price->getNumber(),
      'currency' => $price->getCurrencyCode(),
    ]);
  }

}
