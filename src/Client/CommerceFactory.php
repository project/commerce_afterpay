<?php declare(strict_types=1);

namespace Drupal\commerce_afterpay\Client;

use Afterpay\SDK\Model\Money;
use Afterpay\SDK\Model\Payment;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;

/**
 * Creates Drupal Commerce entities from Afterpay ones.
 *
 * @todo Rename: it isn't really a factory since it started populating existing
 *   objects.
 */
class CommerceFactory {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @var \Drupal\commerce_afterpay\Client\RemoteModel
   */
  protected $remoteModel;

  public function __construct() {
    // TODO: INJECT DEPS
    $this->entityTypeManager = \Drupal::entityTypeManager();
    $this->remoteModel = \Drupal::service('commerce_afterpay.remote.model');
  }

  public function createPrice(Money $money): Price {
    return new Price($money->getAmount(), $money->getCurrency());
  }

  /**
   * Creates a payment based on the last Afterpay payment event.
   *
   * @param \Afterpay\SDK\Model\Payment $payment
   * @param string $order_id
   * @param string $payment_gateway
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createPayment(Payment $payment, string $order_id, string $payment_gateway): PaymentInterface {
    $commerce_payment = $this->entityTypeManager->getStorage('commerce_payment')
      ->create([
        'payment_gateway' => $payment_gateway,
        'order_id' => $order_id,
      ]);
    assert($commerce_payment instanceof PaymentInterface);
    $this->populatePayment($payment, $commerce_payment);
    return $commerce_payment;
  }

  /**
   * Populates an existing payment based on the last Afterpay payment event.
   * It ALSO sets the amount to the last payment event.
   *
   * @param \Afterpay\SDK\Model\Payment $afterpay_payment
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $commerce_payment
   */
  public function populatePayment(Payment $afterpay_payment, PaymentInterface $commerce_payment) {
    $events = $afterpay_payment->getEvents();
    $event = end($events);
    $commerce_payment->setRemoteId($event->getId())
      ->setRemoteState($afterpay_payment->getPaymentState())
      ->setAmount($this->createPrice($event->getAmount()));
  }

}
