<?php declare(strict_types=1);

namespace Drupal\commerce_afterpay\PluginForm;

use Drupal\commerce_afterpay\Plugin\Commerce\PaymentGateway\AfterpayGateway;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Afterpay offsite payment form.
 */
class AfterpayOffsiteForm extends PaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $payment = $this->getEntity();
    assert($payment instanceof PaymentInterface);
    $gateway = $this->plugin;
    assert($gateway instanceof AfterpayGateway);
    // TODO: Do I need to catch exceptions
    $response = $gateway->createCheckout($payment, $form['#return_url'], $form['#cancel_url']);
    return $this->buildRedirectForm($form, $form_state, $response->redirectCheckoutUrl, []);
  }

}
