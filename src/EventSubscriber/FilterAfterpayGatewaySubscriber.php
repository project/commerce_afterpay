<?php declare(strict_types=1);

namespace Drupal\commerce_afterpay\EventSubscriber;

use Drupal\commerce_afterpay\Plugin\Commerce\PaymentGateway\AfterpayGateway;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Event\FilterPaymentGatewaysEvent;
use Drupal\commerce_payment\Event\PaymentEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Ensures that Afterpay isn't offered for orders that are too large or small.
 */
class FilterAfterpayGatewaySubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\commerce_afterpay\Client\ConfigurationFactory
   */
  protected $remoteConfigFactory;

  public function __construct() {
    // TODO: INJECT DEPS
    $this->remoteConfigFactory = \Drupal::service('commerce_afterpay.factory.remote_config');
  }

  /**
   * @inheritDoc
   */
  public static function getSubscribedEvents() {
    return [
      PaymentEvents::FILTER_PAYMENT_GATEWAYS => 'onFilterPaymentGateways',
    ];
  }

  /**
   * Filters out Afterpay gateways for orders that are too large or small.
   *
   * @param \Drupal\commerce_payment\Event\FilterPaymentGatewaysEvent $event
   *   The event.
   */
  public function onFilterPaymentGateways(FilterPaymentGatewaysEvent $event) {
    $order = $event->getOrder();
    $gateways = array_filter($event->getPaymentGateways(), function (PaymentGatewayInterface $gateway) use ($order): bool {
      if ($gateway->getPluginId() !== 'afterpay') {
        return TRUE;
      }
      $client_config = AfterpayGateway::createClientConfig($gateway->getPluginConfiguration());
      $config = $this->remoteConfigFactory->getConfiguration($gateway->id(), $client_config);
      $min = $config->getMinimumAmount();
      $max = $config->getMaximumAmount();
      $total = $order->getTotalPrice();
      // Only allow the gateway if the order's using the Afterpay store
      // currency.
      if ($max->getCurrencyCode() !== $total->getCurrencyCode()) {
        return FALSE;
      }
      return $min->lessThanOrEqual($total) && $total->lessThanOrEqual($max);
    });
    $event->setPaymentGateways($gateways);
  }

}
