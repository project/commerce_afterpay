<?php declare(strict_types=1);

namespace Drupal\commerce_afterpay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_afterpay\Client\Client;
use Drupal\commerce_afterpay\Client\ClientConfig;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Afterpay payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "afterpay",
 *   label = @Translation("Afterpay"),
 *   display_label = @Translation("Afterpay"),
 *   forms = {
 *     "offsite-payment" = "\Drupal\commerce_afterpay\PluginForm\AfterpayOffsiteForm",
 *   },
 * )
 */
class AfterpayGateway extends OffsitePaymentGatewayBase implements SupportsAuthorizationsInterface, SupportsRefundsInterface {

  /**
   * The Afterpay client.
   *
   * @var \Drupal\commerce_afterpay\Client\Client
   */
  private $client;

  /**
   * The Afterpay client factory.
   *
   * @var \Drupal\commerce_afterpay\Client\ClientFactory
   */
  protected $clientFactory;

  /**
   * @var \Drupal\commerce_afterpay\Client\CommerceFactory
   */
  protected $commerceFactory;

  /**
   * @var \Drupal\commerce_afterpay\Client\RemoteModel
   */
  protected $remoteModel;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $gateway = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $gateway->clientFactory = $container->get('commerce_afterpay.factory.client');
    $gateway->commerceFactory = $container->get('commerce_afterpay.factory.commerce');
    $gateway->remoteModel = $container->get('commerce_afterpay.remote.model');
    return $gateway;
  }

  public static function createClientConfig(array $config): ClientConfig {
    return new ClientConfig($config['merchant_id'], $config['secret_key'], $config['country_code'], $config['mode']);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'merchant_id' => '',
      'secret_key' => '',
      'country_code' => 'US',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Merchant ID"),
      '#default_value' => $this->configuration['merchant_id'],
    ];
    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Secret key"),
      '#default_value' => $this->configuration['secret_key'],
    ];
    // @todo Add a select list from the relevant service.
    $form['country_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Country code"),
      '#default_value' => $this->configuration['country_code'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    foreach (['country_code', 'merchant_id', 'secret_key'] as $key) {
      $this->configuration[$key] = $values[$key];
    }
  }

  public function createCheckout(PaymentInterface $payment, string $confirm_url, string $cancel_url): object {
    return $this->getClient()->createCheckout($payment->getOrder(), $confirm_url, $cancel_url);
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    parent::onReturn($order, $request);

    $status = $request->get('status');
    if ($status !== 'SUCCESS')  {
      // Afterpay shouldn't return to this URL with a status of SUCCESS, it's
      // probably a logic error somewhere.
      throw new DeclineException('onReturn status: @status', ['@status' => $status]);
    }

    $token = $request->get('orderToken');
    $payment = $this->getClient()->auth($token);

    if ($payment->getStatus() !== 'APPROVED') {
      throw new DeclineException('Payment status: @status', ['@status' => $payment->getStatus()]);
    }

    // Track the remote order ID.
    $order->setData('commerce_afterpay_order_id', $payment->getId());
    $order->save();

    assert($payment->getPaymentState() === 'AUTH_APPROVED');

    if (count($payment->getEvents()) > 1) {
      throw new PaymentGatewayException(t('Afterpay auth returned more than one payment event.'));
    }

    $commerce_payment = $this->commerceFactory->createPayment($payment, $order->id(), $this->parentEntity->id());
    $commerce_payment->setState('authorization')
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);
    $order->unsetData('commerce_afterpay_order_id');
    // QUESTION: Is there a way/need to cancel an order Afterpay side?
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    // Drupal Commerce doesn't directly support partial capture. The
    // conceptually right model would be to have Authorization entities that
    // result in a Payment entity created for each capture. It was decided to
    // have only the Payment entity for simplicity reasons, and knowingly
    // sacrifice this use case knowing that it was rare, so gateways that need
    // it will need to simulate that flow.
    //
    // Here's the workaround:
    // 1. capturePayment() duplicates the payment, gives it a capture_complete
    //    state and sets its amount to the capture amount. Saves it.
    // 2. The originally passed payment's amount is reduced by the capture
    //    amount, and it too is then saved.
    // 3. If the original payment's amount is depleted, we change its status to
    //    authorization_voided.
    //
    // The flow goes:
    // #1 payment, $100, 'authorization' state
    // | Capture $40
    // #1 payment, $60, 'authorization' state
    // #2 payment, $40, 'capture_complete' state
    // | Capture 60$
    // #1 payment $0, 'authorization_voided' state
    // #2 payment, $40, 'capture_complete' state
    // #3 payment, $60, 'capture_complete' state
    //
    // @see https://www.drupal.org/project/commerce/issues/2847378#comment-12115052

    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    $afterpay_payment = $this->getClient()->capture($payment, $amount);
    if ($afterpay_payment->getPaymentState() === 'CAPTURED') {
      $payment->setAmount($amount);
      $payment->setState('completed');
      $payment->save();
    }
    if ($afterpay_payment->getPaymentState() === 'PARTIALLY_CAPTURED') {

      $new_payment = $payment->createDuplicate();
      $this->commerceFactory->populatePayment($afterpay_payment, $new_payment);
      $new_payment->setState('completed');
      $payment->setAmount($payment->getAmount()->subtract($amount));
      $new_payment->save();
      $payment->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->getClient()->void($payment);
    $payment->setState('authorization_voided')->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->getClient()->refund($payment, $amount);

    // Determine whether payment has been fully or partially refunded.
    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }


  /**
   * Gets the Afterpay client.
   *
   * @return \Drupal\commerce_afterpay\Client\Client
   *   The client.
   */
  protected function getClient(): Client {
    if (!$this->client) {
      $client_config = static::createClientConfig($this->configuration);
      $this->client = $this->clientFactory->createClient($client_config);
    }
    return $this->client;
  }

}
