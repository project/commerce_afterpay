# Commerce Afterpay

The module provides an integration with the Afterpay payment provider.

It's currently limited to the [deferred payment flow] (choosing _Authorize and
capture_) from the payment process pane has no effect).

## Testing against the sandbox API

There's a functional javascript test that can be run to test the checkout and
payment capture/void process. The test assumes that Afterpay will accept a total
of ~45 USD.

1. Set up an account on the sandbox portal (eg. at
   https://portal.sandbox.afterpay.com/us). You'll need to set the password and
   it should already have a single saved payment method.
1. Set the following environment variables:
   - `COMMERCE_AFTERPAY_TEST_MERCHANT_ID`
   - `COMMERCE_AFTERPAY_TEST_SECRET_KEY`
   - `COMMERCE_AFTERPAY_TEST_COUNTRY_CODE`
   - `COMMERCE_AFTERPAY_TEST_CUSTOMER_EMAIL`
   - `COMMERCE_AFTERPAY_TEST_CUSTOMER_PASSWORD`

[deferred payment flow]: https://developers.afterpay.com/afterpay-online/reference#deferred-payment-flow
